﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Obecné informace o sestavení jsou řízeny prostřednictvím následující 
// sady atributů. Chcete-li změnit informace související se sestavením,
// změňte hodnoty těchto atributů.
[assembly: AssemblyTitle("WebApplication1.Tests")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("WebApplication1.Tests")]
[assembly: AssemblyCopyright("Copyright ©  2021")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Nastavení atributu ComVisible na hodnotu false má za výsledek, že typy v tomto sestavení nejsou viditelné 
// pro komponenty modelu COM. Potřebujete-li přistupovat k typu v tomto sestavení 
// z modelu COM, nastavte atribut ComVisible pro daný typ na hodnotu true.
[assembly: ComVisible(false)]

// Následující GUID je určen pro ID knihovny typelib, pokud je tento projekt zpřístupněn modulu COM.
[assembly: Guid("134405a8-7d4a-4ddc-9ece-6c665fb055c1")]

// Informace o verzi sestavení jsou tvořeny následujícími čtyřmi hodnotami:
//
//      Hlavní verze
//      Podverze 
//      Číslo sestavení
//      Revize
//
// Můžete zadat všechny tyto hodnoty, nebo použít výchozí hodnoty pro číslo sestavení a revize 
// zadáním znaku * jako v příkladu níže:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
