﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApplication1.Controllers
{
    public class CustomerController : ApiController
    {
        // GET: api/Customer
        public HttpResponseMessage Get()
        {
            List<customer> result;
            using (var db = new AppDbModel())
            {
                var customers = from customer in db.customers select customer;
                result = customers.OrderByDescending(o => o.name).ToList();
                var jsonString = JsonConvert.SerializeObject(result);

                var resp = new HttpResponseMessage()
                {
                    Content = new StringContent(jsonString)
                };
                resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                return resp;
            }
        }

        // GET: api/Customer/5
        public HttpResponseMessage Get(int id)
        {
            customer customer;
            using (var db = new AppDbModel())
            {
                customer = (from c in db.customers where c.id == id select c).SingleOrDefault();
                var jsonString = JsonConvert.SerializeObject(customer);

                var resp = new HttpResponseMessage()
                {
                    Content = new StringContent(jsonString)
                };
                resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                return resp;
            }
        }

        // POST: api/Customer
        public void Post(HttpRequestMessage value)
        {
            var customer = JsonConvert.DeserializeObject<customer>(value.Content.ReadAsStringAsync().Result);
            using (var db = new AppDbModel())
            {
                db.customers.Add(customer);
                db.SaveChanges();
            }
        }

        // PUT: api/Customer/5
        public void Put(int id, HttpRequestMessage value)
        {
            var requestCustomer = JsonConvert.DeserializeObject<customer>(value.Content.ReadAsStringAsync().Result);
            using (var db = new AppDbModel())
            {
                var customer = (from r in db.customers where r.id == id select r).SingleOrDefault();
                customer.is_active = requestCustomer.is_active;
                customer.is_bussiness_customer = requestCustomer.is_bussiness_customer;
                customer.name = requestCustomer.name;
                customer.email = requestCustomer.email;
                db.SaveChanges();
            }
        }

        // DELETE: api/Customer/5
        public void Delete(int id)
        {
            using (var db = new AppDbModel())
            {
                var customer = (from c in db.customers where c.id == id select c).SingleOrDefault();
                db.customers.Remove(customer);
                db.SaveChanges();
            }
        }
    }
}
