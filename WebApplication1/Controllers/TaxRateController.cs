﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApplication1.Controllers
{
    public class TaxRateController : ApiController
    {
        // GET: api/TaxRate
        public HttpResponseMessage Get()
        {
            List<tax_rate> result;
            using (var db = new AppDbModel())
            {
                var rates = from rate in db.tax_rate select rate;
                result = rates.OrderByDescending(o => o.name).ToList();
                var jsonString = JsonConvert.SerializeObject(result);

                var resp = new HttpResponseMessage()
                {
                    Content = new StringContent(jsonString)
                };
                resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                return resp;
            }
        }

        // GET: api/TaxRate/5
        public HttpResponseMessage Get(int id)
        {
            tax_rate rate;
            using (var db = new AppDbModel())
            {
                rate = (from b in db.tax_rate where b.id == id select b).SingleOrDefault();
                var jsonString = JsonConvert.SerializeObject(rate);

                var resp = new HttpResponseMessage()
                {
                    Content = new StringContent(jsonString)
                };
                resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                return resp;
            }
        }

        // POST: api/TaxRate
        public void Post(HttpRequestMessage value)
        {
            var rate = JsonConvert.DeserializeObject<tax_rate>(value.Content.ReadAsStringAsync().Result);
            using (var db = new AppDbModel())
            {
                db.tax_rate.Add(rate);
                db.SaveChanges();
            }
        }

        // PUT: api/TaxRate/5
        public void Put(int id, HttpRequestMessage value)
        {
            var requestRate = JsonConvert.DeserializeObject<tax_rate>(value.Content.ReadAsStringAsync().Result);
            using (var db = new AppDbModel())
            {
                var rate = (from r in db.tax_rate where r.id == id select r).SingleOrDefault();
                rate.code = requestRate.code;
                rate.name = requestRate.name;
                rate.rate = requestRate.rate;
                db.SaveChanges();
            }
        }

        // DELETE: api/TaxRate/5
        public void Delete(int id)
        {
            using (var db = new AppDbModel())
            {
                var rate = (from r in db.tax_rate where r.id == id select r).SingleOrDefault();
                db.tax_rate.Remove(rate);
                db.SaveChanges();
            }
        }
    }
}
