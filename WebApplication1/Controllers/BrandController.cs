﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApplication1.Controllers
{
    public class BrandController : ApiController
    {
        // GET: api/Brand
        public HttpResponseMessage Get()
        {
            List<brand> result;
            using (var db = new AppDbModel())
            {
                var brands = from brand in db.brands select brand;
                result = brands.OrderByDescending(o => o.name).ToList();
                var jsonString = JsonConvert.SerializeObject(result);

                var resp = new HttpResponseMessage()
                {
                    Content = new StringContent(jsonString)
                };
                resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                return resp;
            }
        }

        // GET: api/Brand/5
        public HttpResponseMessage Get(int id)
        {
            brand brand;
            using (var db = new AppDbModel())
            {
                brand = (from b in db.brands where b.id == id select b).SingleOrDefault();
                var jsonString = JsonConvert.SerializeObject(brand);

                var resp = new HttpResponseMessage()
                {
                    Content = new StringContent(jsonString)
                };
                resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                return resp;
            }
        }

        // POST: api/Brand
        public void Post(HttpRequestMessage value)
        {
            var brand = JsonConvert.DeserializeObject<brand>(value.Content.ReadAsStringAsync().Result);
            using (var db = new AppDbModel())
            {
                db.brands.Add(brand);
                db.SaveChanges();
            }
        }

        // PUT: api/Brand/5
        public void Put(int id, HttpRequestMessage value)
        {
            var requestBrand = JsonConvert.DeserializeObject<brand>(value.Content.ReadAsStringAsync().Result);
            using (var db = new AppDbModel())
            {
                var brand = (from b in db.brands where b.id == id select b).SingleOrDefault();
                brand.code = requestBrand.code;
                brand.name = requestBrand.name;
                brand.tax_rate = requestBrand.tax_rate;
                db.SaveChanges();
            }
        }

        // DELETE: api/Brand/5
        public void Delete(int id)
        {
            using (var db = new AppDbModel())
            {
                var brand = (from b in db.brands where b.id == id select b).SingleOrDefault();
                db.brands.Remove(brand);
                db.SaveChanges();
            }
        }
    }
}
