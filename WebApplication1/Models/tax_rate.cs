namespace WebApplication1
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("bivaa_example_eshop.tax_rate")]
    public partial class tax_rate
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tax_rate()
        {
            brands = new HashSet<brand>();
        }

        public int id { get; set; }

        [Required]
        [StringLength(128)]
        public string code { get; set; }

        [Required]
        [StringLength(255)]
        public string name { get; set; }

        public double rate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        [JsonIgnore]
        public virtual ICollection<brand> brands { get; set; }
    }
}
