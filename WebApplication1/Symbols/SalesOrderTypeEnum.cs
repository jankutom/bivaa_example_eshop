﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Symbols
{
    public enum SalesOrderTypeEnum
    {
        SalesOrder,
        Quotation,
        Proforma
    }
}